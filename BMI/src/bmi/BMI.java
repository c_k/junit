package bmi;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;

public class BMI implements Serializable
{
	static final long serialVersionUID = BMI.class.getClass().hashCode(); 
	
	
	private int koerperGroesseInCM=0;
	private double koerperGewicht=0;
	boolean weiblich = false;
	double bmi;
	//
	public BMI(int kGr,double kGw,boolean fem)
	{
		this.koerperGroesseInCM = kGr;
		this.koerperGewicht = kGw;
		this.weiblich=fem;
	}
	
	//in unit eine neue datei erstellen die von bmi erbt und 

	
	// + toString, hashcode und equals überschreiben
	@Override
	public String toString()
	{
		return String.valueOf(getBMI());
	}
		
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		long temp;
		temp = Double.doubleToLongBits(bmi);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		BMI other = (BMI) obj;
		if (Double.doubleToLongBits(bmi) != Double.doubleToLongBits(other.bmi))
			return false;
		return true;
	}

	public void initObjStreamWriter()
	{
		ObjectOutputStream objStream=null;
		try
		{
			objStream = new ObjectOutputStream(new FileOutputStream("bmi.tmp"));
			objStream.writeObject(this.koerperGewicht);
			objStream.writeObject(this.koerperGroesseInCM);
			objStream.writeObject(this.bmi);
		}
		catch(IOException e404)
		{
			System.err.println(e404.getMessage());
		}
		finally
		{
			try 
			{
				objStream.close();
			} 
			catch (IOException e)
			{
				e.printStackTrace();
			}
			finally
			{
				
			}
		}
	}
	
	public void getGeschlecht()
	{	
		String geschlecht="male";
		
		if(weiblich)
		{
			geschlecht="femnale";
			System.out.println(geschlecht);
		}
		System.out.println(geschlecht);
	}
	
	public double getBMI()
	{
			bmi = (koerperGewicht/Math.pow(((double)koerperGroesseInCM/100),2));
			return bmi;		
	}

	

	public int getKoerperGroesseInCM() {
		return this.koerperGroesseInCM;
	}

	public double getKoerperGewicht() {
		return this.koerperGewicht;
	}

	public boolean isWeiblich() {
		return weiblich;
	}
	
}
