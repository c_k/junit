package ui;

import java.util.Scanner;

public class UserInput 
{
	private static String userInput = "";
	private static Double dui = new Double(0);
	private static Integer iui = new Integer(0);

	private Scanner sc = new Scanner(System.in);

	public String getUserInput(String intCommand) {
		System.out.println(intCommand);
		this.userInput = sc.nextLine();
		return userInput;
	}

	public Double getDui(String intCommand) {
		System.out.println(intCommand);
		this.dui = sc.nextDouble();
		return dui;
	}

	public Integer getIui(String intCommand) {
		System.out.println(intCommand);
		this.iui=sc.nextInt();
		return iui;
	}
		
}
